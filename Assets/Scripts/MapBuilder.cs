﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class MapBuilder : MonoBehaviour {
	public const int CHUNKS = 7;// Only supports odd numbers
	public static int _width = 257;
	public static int _length = 257;
	public static int _height = 60;
	public GameObject player;
	public MeshRenderer waterQuad;
	public GameObject playerCamera;
	public GameObject WaterPrefab;
	public MeshRenderer UnderwaterRenderer;

    public Vector3 chunkSize = new Vector3(_width, _height, _length);
	public int HeightMapRes = 33;


	// Noise Objects
	private SimplexNoise2D sNoise;
	private SimplexNoise2D detailSNoise;
	private DiamondSquare  dNoise;
	
	private float[,,]     alphaMap;
	private EnvDetail[][,]envMaps    = new EnvDetail[CHUNKS * CHUNKS][,];
	private Terrain[,]    terrains   = new Terrain[CHUNKS,CHUNKS];
	private float[][,]    heightMaps = new float[CHUNKS * CHUNKS][,];
	
	private Terrain[]      TerrainsBuffer  = new Terrain[CHUNKS];
	private float [][,]    HeightMapBuffer = new float[CHUNKS][,];
	private EnvDetail[][,] envBuffer       = new EnvDetail[CHUNKS][,];
	
	
	private int[][,]       detailMaps;	
	
	
	public float scale             = 0.01f;
	public float SDAverageFactor   = 1.6f;
	public float WaterTable        = .14f;
	
	public  float TreeBillboardDist = 70f;
	public  float TreeDistance      = 1000f;
	public  float CrossFadeLength   = 60f;
	public  int   MaxTreeCount      = 60;
	private int   CenterTile = Mathf.FloorToInt(CHUNKS/2);

	public BiomeRule[] BiomeRules;

	public SplatPrototype [] splats;
	public TreePrototype  [] trees;
	public DetailPrototype[] details;

	public TextureRule[]  presortedTextureRules;

	public TextureRule[][] textureRules;
	public TreeRule   [] treeRules;

	/// [biome][index]
	public TextureRule[][] TextureRules;
	public TreeRule   [][] TreeRules;

    // Simplex
    public SimplexAttributes simplex = new SimplexAttributes();
	public SimplexAttributes treeSimplex = new SimplexAttributes();
	public SimplexAttributes temperatureSimplex = new SimplexAttributes();
	public SimplexAttributes humiditySimplex = new SimplexAttributes();
	
	// Diamond
	public DiamondAttributes diamond = new DiamondAttributes();

	// Invokes before everything else does.
	void Start () {
		initNoises();
		loadSplat();
		loadDetails ();
		createBaseTerrains();
	}

	void Update(){
		// Swap on height.
		UnderwaterRenderer.enabled = waterQuad.enabled = 
			playerCamera.transform.position.y < WaterPrefab.transform.position.y;

		if(isNotInCenterTile()){
			shiftTiles();
		}
	}

	private bool isNotInCenterTile(){
		return (
			player.transform.position.x > terrains[CenterTile, CenterTile].transform.position.x + chunkSize.x || // Left1 < Right2
			player.transform.position.x < terrains[CenterTile, CenterTile].transform.position.x     		  || // Right1 > Left2
			player.transform.position.z > terrains[CenterTile, CenterTile].transform.position.z + chunkSize.z || // Top1 < Bottom2
			player.transform.position.z < terrains[CenterTile, CenterTile].transform.position.z);                // Bottom1 > Top2 
	}

	private void loadSplat(){
		int splatIndex = 0;

		ArrayList[] bins = new ArrayList[Enum.GetNames(typeof(Biome)).Length];
		textureRules = new TextureRule[bins.Length][];
		splats = new SplatPrototype[presortedTextureRules.Length];

		for(int i = 0; i < bins.Length; i++){
			bins[i] = new ArrayList();
		}

		for(int i = 0; i < presortedTextureRules.Length; i++){
			bins[(int)presortedTextureRules[i].biome].Add(presortedTextureRules[i]);
		}

		for(int i = 0; i < bins.Length; i++){
			textureRules[i] = (TextureRule[])bins[i].ToArray(typeof(TextureRule));
		}

		for(int index = 0; index <  textureRules.Length;index++){
			for(int indexIn = 0; indexIn < textureRules[index].Length;indexIn++,splatIndex++){
				textureRules[index][indexIn].SplatIndex = splatIndex;
				splats[splatIndex] = new SplatPrototype();
				splats[splatIndex].texture = textureRules[index][indexIn].Texture;
				splats[splatIndex].tileSize = textureRules[index][indexIn].TileSize;
				splats[splatIndex].tileOffset = Vector2.zero;
			}
		}
	}

	private void loadDetails(){
		trees = new TreePrototype[treeRules.Length];

		for (int index = 0; index < trees.Length; index++) {
			trees[index] = new TreePrototype();
			trees[index].prefab = treeRules[index].prefab;
		}
	}

	private void moveWater(float x, float y){
		this.WaterPrefab.transform.position = new Vector3(x + chunkSize.x/2,WaterTable*chunkSize.y,y+ chunkSize.z/2);
	}

	private void generateTrees(int row,int col){
		Environment.CreateTrees ((int)(terrains [row, col].transform.position.x/chunkSize.x* HeightMapRes),  
		                         (int)(terrains [row, col].transform.position.z/chunkSize.z* HeightMapRes), 
	                         ref heightMaps [col * CHUNKS + row],  
	                         ref terrains [row, col], 
	                         ref treeRules);
	}

	private void setTerrain(int row, int col){
		int index = col * CHUNKS + row;
		int biome;

		terrains[row,col].terrainData.SetHeights(0, 0, heightMaps[index]);

		for (int i = 0; i < HeightMapRes; i++)
		{
			for (int j = 0; j < HeightMapRes; j++)
			{
				biome= envMaps[index][i,j].biome;
				for(int sIndex = 0; sIndex < splats.Length; sIndex++){
					alphaMap[i, j, sIndex] =0;
				}

				for(int texture = 0; texture < textureRules[0].Length; texture++){
					alphaMap[i, j, textureRules[0][texture].SplatIndex] = 
						textureRules[0][texture].CalculateBlend(heightMaps[index][i, j]);
				}

				if(biome !=0){
					for(int texture = 0; texture < textureRules[biome].Length; texture++){
						alphaMap[i, j, textureRules[biome][texture].SplatIndex] = 
							textureRules[biome][texture].CalculateBlend(heightMaps[index][i, j]);
					}
				}
			}
		}

		terrains[row,col].terrainData.SetAlphamaps(0, 0, alphaMap);
	}

	private void generateMapForIndex(int index, int posX, int posY){
		// Noise and such.
		float[,] dMap = new float[HeightMapRes,HeightMapRes];

		simplexNoise(ref heightMaps[index], posX, posY);
		diamondSquare(ref dMap,posX,posY);
		Combine.AverageBiome(ref heightMaps[index],ref dMap,ref envMaps[index],HeightMapRes,SDAverageFactor);

		applyNormalize(index);
	}

	private void createBaseTerrains(){
		int width = (int)chunkSize.x;
		int height = (int)chunkSize.y;
		int length = (int)chunkSize.z;
		TerrainData tData;
		alphaMap = null;

		for (int r = 0; r < CHUNKS; r++) {
				for (int c = 0; c < CHUNKS; c++) {		
					heightMaps [c * CHUNKS + r] = new float[HeightMapRes, HeightMapRes]; 

					envMaps [c * CHUNKS + r] = new EnvDetail[HeightMapRes, HeightMapRes];

					Environment.HumidityAndTemperature (ref envMaps [c * CHUNKS + r], c * (HeightMapRes), r * (HeightMapRes));

					generateMapForIndex (c * CHUNKS + r, c * (HeightMapRes), r * (HeightMapRes));

					if (r != 0) {
							for (int k = 0; k < HeightMapRes; k++) {
									heightMaps [c * CHUNKS + r] [k, 0] = heightMaps [c * CHUNKS + (r - 1)] [k, HeightMapRes - 1];
							}
					}
	
					if (c != 0) {
							for (int k = 0; k < HeightMapRes; k++) {

									heightMaps [c * CHUNKS + r] [0, k] = heightMaps [(c - 1) * CHUNKS + r] [HeightMapRes - 1, k];
							}
					}
				}
		}

		for (int row = 0; row < CHUNKS; row++) {
				for (int col = 0; col < CHUNKS; col++) {		
					tData = new TerrainData ();
					tData.SetDetailResolution (width, 16);

					tData.heightmapResolution = HeightMapRes;
					tData.alphamapResolution = HeightMapRes;

					tData.baseMapResolution = width;
					tData.size = chunkSize;
					tData.splatPrototypes = splats;
					tData.detailPrototypes = details;
					tData.treePrototypes = trees;

					if (alphaMap == null) {
							alphaMap = tData.GetAlphamaps (
								0, 0, 
								tData.alphamapWidth,
								tData.alphamapHeight);
					}


					terrains [row, col] = (Terrain)Terrain.CreateTerrainGameObject (tData).GetComponent ("Terrain");
					terrains [row, col].transform.position = new Vector3 (row * (width), 0, col * (length));
					terrains [row, col].treeBillboardDistance = TreeBillboardDist;
					terrains [row, col].treeCrossFadeLength = CrossFadeLength;
					terrains [row, col].treeDistance = TreeDistance;

					generateTrees (row, col);
					setTerrain (row, col);
					terrains [row, col].heightmapPixelError = 0;
					terrains [row, col].Flush ();
				}	
		}
		moveWater (terrains [CenterTile, CenterTile].transform.position.x, terrains [CenterTile, CenterTile].transform.position.z);

		player.transform.position = terrains [CenterTile, CenterTile].transform.position;
		player.transform.Translate (width / 2, height + 20, length / 2);

		RaycastHit hit;
		if (Physics.Raycast (player.transform.position, Vector3.down,out hit, Mathf.Infinity)) {
			player.transform.position = hit.point;
			player.transform.Translate(0,1f,0f);
		}

	}

	/// <summary>
	/// Inits the noises.
	/// </summary>
	private void initNoises(){
		this.sNoise = new SimplexNoise2D ();//42);
		this.dNoise = new DiamondSquare ();//700);

		Environment.init (HeightMapRes, ref treeSimplex, ref temperatureSimplex, ref humiditySimplex, ref BiomeRules);//, 42);
	}

	private void simplexNoise(ref float[,] map, int posX, int posY){
		for(int x = 0; x < HeightMapRes; x++){
			for(int y = 0; y < HeightMapRes; y++){
				map[x,y] = this.sNoise.height2d((x + posX)*scale, (y + posY)*scale, ref simplex) * simplex.scale + simplex.yOffset;
			}
		}	

		/// This reduces the edge artifacts to a tolerable limit.
		for(int edgeIndex = 0; edgeIndex < HeightMapRes; edgeIndex++){
			map[0,edgeIndex] = (map[0,edgeIndex] + 
			                    this.sNoise.height2d((posX-1)*scale, (edgeIndex + posY)*scale, ref simplex)* simplex.scale + simplex.yOffset)/2;
			map[HeightMapRes-1,edgeIndex] = (map[HeightMapRes-1,edgeIndex] + 
			                                 this.sNoise.height2d((HeightMapRes + posX)*scale, (edgeIndex + posY)*scale, ref simplex)* simplex.scale + simplex.yOffset)/2;

			map[edgeIndex,0] = (map[edgeIndex,0] + 
			                    this.sNoise.height2d((edgeIndex + posX)*scale, (posY-1)*scale, ref simplex)* simplex.scale + simplex.yOffset)/2;
			map[edgeIndex,HeightMapRes-1] = (map[edgeIndex,HeightMapRes-1] + 
			                                 this.sNoise.height2d((edgeIndex + posX)*scale, (HeightMapRes + posY)*scale, ref simplex)* simplex.scale + simplex.yOffset)/2;

		}
	}
	
	private void diamondSquare(ref float[,]map, int posX, int posY){
		this.dNoise.noise(ref map, posX, posY, HeightMapRes, ref diamond);
	}
	
	private void applyNormalize(int index, float highestPoint = 1.0f, float lowestPoint = -1.0f,  
	                   float normalisedHeightRange = 1.0f,float normaliseMin= 0.0f){
		for(int x = 0; x < HeightMapRes; x++){
			for(int y = 0; y < HeightMapRes; y++){
				heightMaps[index][x,y] = normaliseMin + (((heightMaps[index][x,y] - lowestPoint) / (highestPoint - lowestPoint)) * normalisedHeightRange);
			}
		}
	}

	private void shiftTiles(){
		int width = (int)chunkSize.x;
		int length = (int)chunkSize.z;
		int tiles = CHUNKS * CHUNKS;
		short positionShift = -1; // 0 - row, 1 - row, 2 - col, 3 - col
		int colShift = 0;
		int rowShift = 0;
		int rawIndexMod = 0;
		int indexBuffer = 0;
		/*
         * z
         * 2 | 6 7 8  
         * 1 | 3 4 5 
         * 0 | 0 1 2
         *    -------
         *     0 1 2  x 
         *  p0: 4:1 z:2 -> z:0 saves: z:2 6,7,8
         *  p1: 4:7 z:0 -> z:2 saves: z:0 0,1,2 
         *  p2: 4:3 x:2 -> x:0 saves: x:2 2,5,8
         *  p3: 4:5 x:0 -> z:2 saves: x:0 0,3,6 
         */
		if (player.transform.position.z < terrains[CenterTile, CenterTile].transform.position.z) // DOWN
		{
			positionShift = 0;
			rawIndexMod = CHUNKS;
			rowShift = 1;
		}
		else if (player.transform.position.z > terrains[CenterTile, CenterTile].transform.position.z + chunkSize.z) // TOP
		{
			positionShift = 1;
			rawIndexMod = -CHUNKS;
			rowShift = -1;
			
		}
		else if (player.transform.position.x < terrains[CenterTile, CenterTile].transform.position.x) // Left
		{
			positionShift = 2;
			rawIndexMod = 1;
			colShift = 1;
			
		}
		else if (player.transform.position.x > terrains[CenterTile, CenterTile].transform.position.x + chunkSize.x) // Right
		{
			positionShift = 3;
			rawIndexMod = -1;
			colShift = -1;
		}
		else
			return;
		
		// Save Verified
		if (positionShift > 1)
		{
			for (int bak_index = positionShift == 3 ? 0 : CHUNKS - 1; bak_index < tiles; bak_index += CHUNKS)
			{
				TerrainsBuffer  [Mathf.FloorToInt(bak_index / CHUNKS)] = terrains[bak_index % CHUNKS, Mathf.FloorToInt(bak_index / CHUNKS)];
				terrains[bak_index % CHUNKS, Mathf.FloorToInt(bak_index / CHUNKS)] = null;
				
				HeightMapBuffer[Mathf.FloorToInt(bak_index / CHUNKS)] = heightMaps[bak_index];
				heightMaps[bak_index] = null;

				envBuffer[Mathf.FloorToInt(bak_index / CHUNKS)] = envMaps[bak_index];
				envMaps[bak_index] = null;
			}
		}
		else
		{
			int positionLimit = positionShift == 0 ? tiles:CHUNKS;
			for (int bak_index = positionShift == 0 ? tiles - CHUNKS: 0; bak_index < positionLimit; bak_index++)
			{
				TerrainsBuffer  [bak_index % CHUNKS] = terrains[bak_index % CHUNKS, Mathf.FloorToInt(bak_index / CHUNKS)];
				terrains[bak_index % CHUNKS, Mathf.FloorToInt(bak_index / CHUNKS)] = null;
				
				HeightMapBuffer[bak_index % CHUNKS] = heightMaps[bak_index];
				heightMaps[bak_index] = null;

				envBuffer[bak_index % CHUNKS] = envMaps[bak_index];
				envMaps[bak_index] = null;
			}
		}		
		
		// Shift Verified
		if (positionShift % 2 == 0) 
		{
			for (int index = tiles - 1; index >= 0; index--)
			{
				if ((positionShift == 0 && index < tiles - CHUNKS) ||
				    (positionShift == 2 && index % CHUNKS != CHUNKS-1))
				{
					// Shift array index HM
					heightMaps[index + rawIndexMod] = heightMaps[index];

					envMaps[index + rawIndexMod] =  envMaps[index];

					// Shift Array index terrains
					terrains[index % CHUNKS + colShift, Mathf.FloorToInt(index / CHUNKS) + rowShift] = terrains[index % CHUNKS, Mathf.FloorToInt(index / CHUNKS)];
				}
			}
		}
		else
		{
			for (int index = 0; index < tiles; index++)
			{
				if ((positionShift == 1 && index >= CHUNKS) ||
				    (positionShift == 3 && index % CHUNKS != 0))
				{
					// Shift array index HM
					heightMaps[index + rawIndexMod] = heightMaps[index];

					envMaps[index + rawIndexMod] =  envMaps[index];

					// Shift Array index terrains
					terrains[index % CHUNKS + colShift, Mathf.FloorToInt(index / CHUNKS) + rowShift] = terrains[index % CHUNKS, Mathf.FloorToInt(index / CHUNKS)];
				}
			}
		}
		
		// Recover Verified
		if (positionShift > 1)
		{
			for (int bak_index = 0; bak_index < CHUNKS; bak_index++)
			{
				indexBuffer = positionShift == 3 ? (bak_index + 1) * CHUNKS - 1 : bak_index * CHUNKS;
				
				terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)] = TerrainsBuffer[bak_index];
				terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position =
					new Vector3(terrains[indexBuffer % CHUNKS + colShift, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.x + (-colShift * (width)),
					            terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.y,
					            terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.z);
				
				heightMaps[indexBuffer] = HeightMapBuffer[bak_index];

				envMaps[indexBuffer] =  envBuffer[bak_index];


				Environment.HumidityAndTemperature(ref envMaps[indexBuffer],
				                       (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.z/
				                       length * HeightMapRes,
				                       (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.x/
				                       width * HeightMapRes);

				generateMapForIndex(indexBuffer, (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.z/
				                    length * HeightMapRes,
				                    (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.x/
				                    width * HeightMapRes);



				generateTrees(indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS));
				HeightMapBuffer[bak_index] = null;
				TerrainsBuffer[bak_index]  = null;
				envBuffer[bak_index]       = null;
			}
		}
		else
		{
			for (int bak_index = 0; bak_index < CHUNKS; bak_index++)
			{
				indexBuffer = positionShift == 1 ? tiles - CHUNKS + bak_index : bak_index;
				
				terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)] = TerrainsBuffer[bak_index];
				terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position = 
					new Vector3(terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.x,
					            terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.y,
					            terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS) + rowShift].transform.position.z + (-rowShift * (length)));

				heightMaps[indexBuffer]   = HeightMapBuffer[bak_index];

				envMaps[indexBuffer] =  envBuffer[bak_index];

				
				Environment.HumidityAndTemperature(ref envMaps[indexBuffer],
				                       (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.z/
				                       length * HeightMapRes,
				                       (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.x/
				                       width * HeightMapRes);


				generateMapForIndex(indexBuffer, 
				                    (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.z/
				                    	length *  HeightMapRes, 
				                    (int)terrains[indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS)].transform.position.x/
										width * HeightMapRes);

				generateTrees(indexBuffer % CHUNKS, Mathf.FloorToInt(indexBuffer / CHUNKS));

				HeightMapBuffer[bak_index] = null;
				TerrainsBuffer[bak_index]  = null;
				envBuffer[bak_index]       = null;
			}
		}

		for (int r = 0; r < CHUNKS; r++)
		{
			for (int c = 0; c < CHUNKS; c++)
			{	
				if (r != 0)
				{
					for (int k = 0; k < HeightMapRes; k++)
					{
						heightMaps[c * CHUNKS + r][k, 0] = heightMaps[c * CHUNKS + (r-1)][k, HeightMapRes - 1]  ;
					}
				}
				
				if (c != 0 )
				{
					for (int k = 0; k < HeightMapRes; k++)
					{
						heightMaps[c * CHUNKS + r][0, k]  = heightMaps[(c-1) * CHUNKS + r][HeightMapRes - 1, k] ;
					}
				}
			}
		}		

		StartCoroutine(updateTerrainData());	
	}

	private IEnumerator updateTerrainData(){
		for (int r = 0; r < CHUNKS; r++)
		{
			for (int c = 0; c < CHUNKS; c++)
			{	
				setTerrain(r,c);
				if(r == CenterTile && c ==CenterTile){
					moveWater(terrains[CenterTile, CenterTile].transform.position.x, 
					          terrains[CenterTile, CenterTile].transform.position.z);
				}
				yield return null;
			}
		}
	}
}

[System.Serializable]
public class TextureRule {
	public Texture2D Texture = null;
	public Vector2 TileSize;
	public float SteepnessMin = 0f;
	public float SteepnessMax = 1f;
	public float HeightMin = 0f;
	public float HeightMax = 1f;
	public int SplatIndex;
	public Biome biome;
	public float blend = 1f;

	public float CalculateBlend(float height){
		if (height <= HeightMax && height >= HeightMin) {
			return blend;
		}
		else
			return 0;
	}
}
