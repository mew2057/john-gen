﻿using UnityEngine;
using System.Collections;

public class Combine  {

	public static void Multiply(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				baseMap[x,y] *= overlayMap[x,y];
			}
		}
	}

	public static void Add(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				baseMap[x,y] += overlayMap[x,y];
			}
		}
	}



	public static void SubtractFromBase(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){				
				baseMap[x,y] -= overlayMap[x,y];
			}
		}
	}

	public static void SubtractFromOverlay(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){				
				baseMap[x,y] = overlayMap[x,y] - baseMap[x,y] ;
			}
		}
	}

	public static void Divide(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				baseMap[x,y] /= overlayMap[x,y];
			}
		}
	}

	public static void Min(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				baseMap[x,y] = baseMap[x,y] < overlayMap[x,y] ? baseMap[x,y] : overlayMap[x,y];
			}
		}	
	}

	public static void Max(ref float[,] baseMap, ref float[,] overlayMap, int side){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				baseMap[x,y] = baseMap[x,y] > overlayMap[x,y] ? baseMap[x,y] : overlayMap[x,y];
			}
		}	
	}

	public static void MinWithLimit(ref float[,] baseMap, ref float[,] overlayMap, int side, float limit){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				if(baseMap[x,y] > limit)
					baseMap[x,y] = overlayMap[x,y];
				else
					baseMap[x,y] = baseMap[x,y] < overlayMap[x,y] ? baseMap[x,y] : overlayMap[x,y];
			}
		}	
	}

	public static void Average(ref float[,] baseMap, ref float[,] overlayMap, int side, float baseWeight = 1){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){
				baseMap[x,y] = (baseMap[x,y]*baseWeight + overlayMap[x,y])/2f;
			}
		}	
	}

	public static void AverageBiome(ref float[,] baseMap, ref float[,] overlayMap, ref EnvDetail[,] biomes, int side,float baseWeight = 1){
		for (int x = 0; x < side; x++){
			for (int y = 0; y < side; y++){

				baseMap[x,y] = (baseMap[x,y]*baseWeight +
				                overlayMap[x,y] * (1f-((biomes[x,y].humidity)*(biomes[x,y].humidity))))/2f -
					((biomes[x,y].humidity-.5f)*(biomes[x,y].humidity-.5f))/2; // High and Low Humidity will trigger.
			}	
		}
	}

}
