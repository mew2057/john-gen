﻿using UnityEngine;
using System.Collections;

public class Environment:MonoBehaviour  {
	public static System.Random rand;
	public static int seed =0;
	public static SimplexNoise2D tSimplex;
	public static SimplexAttributes treeSimplex;

	public static SimplexNoise2D tempSimplex;
	public static SimplexAttributes temperatureSimplex;

	public static SimplexNoise2D humSimplex;
	public static SimplexAttributes humiditySimplex;

	public static int heightMapRes;
	public static BiomeRule[] BiomeRules;

	public static void init(int heightMapRes, ref SimplexAttributes treeSimplex, 
	                        ref SimplexAttributes tempSimplex, ref SimplexAttributes humSimplex,
	                        ref BiomeRule[] rules,
	                        int newSeed =-1 ){
		if(newSeed !=-1)
			Environment.seed = newSeed;
		else
			Environment.seed = new System.Random().Next();

		Environment.heightMapRes = heightMapRes;


		Environment.tSimplex    = new SimplexNoise2D(Environment.seed);
		Environment.tempSimplex = new SimplexNoise2D(Environment.seed + 1);
		Environment.humSimplex  = new SimplexNoise2D(Environment.seed + 2);

		Environment.treeSimplex = treeSimplex;
		Environment.temperatureSimplex = tempSimplex;
		Environment.humiditySimplex = humSimplex;

		BiomeRules = rules;
	}

	public static void CreateTrees(int posX, int posY,
	                               ref float[,] heightMap, ref Terrain terrain, ref TreeRule[] treeRules){
		rand = new System.Random (Environment.seed * (posX * posX + posY));
		float[,] populationMap = new float[heightMapRes,heightMapRes];

		// Set up the TreeInstance for manipulating.
		TreeInstance treeInstance = terrain.terrainData.treeInstances.Length != 0 ? 
			terrain.terrainData.treeInstances[0] : new TreeInstance();
		treeInstance.prototypeIndex = 0;
		treeInstance.color = Color.white;
		treeInstance.lightmapColor =Color.white;
		treeInstance.heightScale = 1;
		treeInstance.widthScale  = 1;
		treeInstance.position = new Vector3(.5f,0f,.5f);

		float popCenterRad     = 1/(2f*heightMapRes);
		float fX, fY;

		terrain.terrainData.treeInstances = new TreeInstance [0];
		for (int mX = 0; mX < heightMapRes; mX++)
		{
			for (int mY = 0; mY < heightMapRes; mY++)
			{
				populationMap[mX,mY] = Environment.tSimplex.height2d(mX+posX,mY+posY,ref treeSimplex);
			}
		}
		Environment.ApplyNormalize(ref populationMap);


		for (int mX = 0; mX < heightMapRes; mX++)
		{
			for (int mY = 0; mY < heightMapRes; mY++)
			{
				if(treeRules[0].checkHeight(heightMap[mX,mY]) && treeRules[0].checkNoise(populationMap[mX,mY] )){
					fY = (float)mY / heightMapRes;
					fX = (float)mX / heightMapRes;

					treeInstance.position = new Vector3(fY+ popCenterRad - (2 * popCenterRad * (float)rand.NextDouble()), 0, fX+ popCenterRad - (2 * popCenterRad * (float)rand.NextDouble()));
					treeInstance.heightScale = 1.5f;
					treeInstance.widthScale = 1.25f;
					treeInstance.prototypeIndex = 0;
					terrain.AddTreeInstance(treeInstance);
				}
			}
		}
		terrain.Flush ();
	}

	private static void ApplyNormalize(ref float[,] heightMap, float highestPoint = 1.0f, float lowestPoint = -1.0f,  
	                           float normalisedHeightRange = 1.0f,float normaliseMin= 0.0f){
		for(int x = 0; x < heightMapRes; x++){
			for(int y = 0; y < heightMapRes; y++){
				heightMap[x,y] = normaliseMin + (((heightMap[x,y] - lowestPoint) / (highestPoint - lowestPoint)) * normalisedHeightRange);
			}
		}
	}

	public static void HumidityAndTemperature(ref EnvDetail[,] details, float posX, float posY){
		int biome;

		for(int x = 0; x < heightMapRes; x++){
			for(int y = 0; y < heightMapRes; y++){
				details[x,y].humidity    = Environment.humSimplex.height2d(posX+x,posY+y, ref Environment.humiditySimplex);
				details[x,y].temperature = Environment.tempSimplex.height2d(posX+x,posY+y, ref Environment.temperatureSimplex);

				for( biome = Environment.BiomeRules.Length-1; biome > 0 ; biome--){
					if(details[x,y].temperature > Environment.BiomeRules[biome].minTemp && details[x,y].temperature < Environment.BiomeRules[biome].maxTemp ){
						break;
					}
				}

				details[x,y].biome = biome;
			}
		}
	}
}


[System.Serializable]
public class TreeRule{
	public GameObject prefab;
	public float MinHeight = 0f;
	public float MaxHeight = 1f;
	public float NoiseThreshold = .15f;
	public int DensityPerPopulation = 5;
	public int SpecialSplat =-1;

	public bool checkHeight(float height){
		return height > MinHeight  && height < MaxHeight;
	}

	public bool checkNoise(float noise){
		return noise > NoiseThreshold;
	}
}

[System.Serializable]
public enum Biome{
	All=0, Cold
}

[System.Serializable]
public class BiomeRule{
	public float minTemp = 0f, maxTemp = 1f;
}

public struct EnvDetail{
	/// <summary>
	/// A normal distribution of moisture with the minimum being at 0.
	/// -1..1
	/// </summary>
	public float humidity;

	/// <summary>
	/// A linear temperture.
	/// -1..1
	/// </summary>
	public float temperature;

	/// <summary>
	/// The biome affected by the temperature exclusively.
	/// </summary>
	public int biome;
}

