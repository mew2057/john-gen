﻿using UnityEngine;
using System.Collections;

public class DiamondSquare  {
	private int seed;

	// I'm using libnoise's constants
	private static int xCon = 1619;
	private static int yCon = 31337;
	private static int sCon = 13;

	System.Random rand;

	public DiamondSquare(int seed = -1){
		if(seed !=-1)
			this.seed = seed;
		else
			this.seed = new System.Random().Next();

		this.seed*=sCon;
	}

	// 33 is the min for unity's height map.
	public void noise(ref float[,] map, int posX, int posY, int baseSideLen, ref DiamondAttributes attrib){
		float h = attrib.H;
		float normFactor = attrib.maxHeight * attrib.scale;
		int halfSize = 0;
		float avg = 0;
		float offset = 0;
		int arrayLimit = baseSideLen - 1;

		float s0,s1,s2,s3,c;

		map[0,0] 					  = (attrib.maxHeight * getNoise(posX - 1         , posY - 1         )) * attrib.scale;
		map[0,arrayLimit] 			  = (attrib.maxHeight * getNoise(posX - 1         , posY + arrayLimit)) * attrib.scale;
		map[arrayLimit,0]			  = (attrib.maxHeight * getNoise(posX + arrayLimit, posY - 1         )) * attrib.scale;
		map[arrayLimit,arrayLimit]    = (attrib.maxHeight * getNoise(posX + arrayLimit, posY + arrayLimit)) * attrib.scale;

		for(int side = arrayLimit; side>1; side/=2, h/=2){
			halfSize = side/2;
			for (int point = halfSize; point < arrayLimit; point += side){
				map[0,point] = (map[0,point - halfSize] + map[0,point+halfSize]) / 2 +
					h * getNoise(posX - 1, point + posY) * attrib.scale;

				map[arrayLimit,point] = (map[arrayLimit,point - halfSize] + map[arrayLimit,point+halfSize]) / 2 +
					h * getNoise(posX + arrayLimit, point + posY)  * attrib.scale;

				map[point,0] = (map[point - halfSize, 0] + map[point+halfSize,0]) / 2 +
					h * getNoise(posX + point, posY - 1) * attrib.scale;
				
				map[point, arrayLimit] = (map[point - halfSize,arrayLimit] + map[point+halfSize,arrayLimit]) / 2 +
					h * getNoise(posX + point, posY + arrayLimit)  * attrib.scale;
			}
		}

		h = attrib.H;
		// Try calculating 4 surrounding
		for (int side = arrayLimit; side>1; side/=2, h/=2) {
			halfSize = side/2;

			// Square
			for (int x = 0; x < arrayLimit; x += side){
				for (int y = 0; y < arrayLimit; y += side){
					avg = (map[x    , y       ] + 
						map[x       , y + side] +
						map[x + side, y       ] +
						map[x + side, y + side])/4.0f;

					map[x + halfSize, y + halfSize] = avg + ( h * getNoise(posX +x + halfSize,posY +  y + halfSize)) * attrib.scale;
				}
			}

			// Diamond
			for (int x = 0; x < arrayLimit; x += side){
				for (int y = 0; y < arrayLimit; y += side){
					s0 = map[x    	 , y       ];
					s1 = map[x       , y + side];
					s2 = map[x + side, y       ];
					s3 = map[x + side, y + side];
					c = map[x + halfSize, y + halfSize];

					offset = attrib.scale * h * getNoise(posX + x, posY + y + halfSize);

					if(x != 0){
						map[x, y + halfSize] = (x == 0 ? ((s0+s1+c) / 3.0f + offset): 
						                        ((s0+s1+c+map[x - halfSize , y + halfSize] ) / 4.0f + offset));
					}

					offset = attrib.scale * h * getNoise(posX + x + side, posY + y + halfSize);
					if(x + side <  arrayLimit){
						map[x + side, y + halfSize] = (x + side ==  arrayLimit ? (s2+s3+c) / 3.0f: 
					                               (s2+s3+c+map[x + side + halfSize , y + halfSize] ) / 4.0f) +  offset;
					}
						
					offset = attrib.scale * h * getNoise(posX + x + halfSize, posY + y + side);
					if(y != 0){
						map[x + halfSize, y] = (y == 0 ? (s0+s2+c) / 3.0f: 
					                        (s0+s2+c+map[x + halfSize , y - halfSize] ) / 4.0f) + offset;
					}

					offset = attrib.scale * h * getNoise(posX + x + halfSize, posY + y + side);
					if(y + side <  arrayLimit){
						map[x + halfSize, y + side] = (y + side ==  arrayLimit ? (s1+s3+c) / 3.0f: 
					                               (s1+s3+c+map[x + halfSize , y + side + halfSize] ) / 4.0f) + offset;  
					}
				}
			}
		}

		// Doesn't perfectly normalize, but I like the imperfections, they are nice as character points in the terrain.
		for (int x = 0; x < baseSideLen; x++){
			for (int y = 0; y < baseSideLen; y++){
				map[x, y] /= normFactor;
			}
		}
	}

	/// <summary>
	/// Gets the noise. A modification of libnoise's Value3D noise. [-1..1]
	/// </summary>
	/// <returns>The noise.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	public float getNoise(int x, int y){
		// 0x7fffffff is int max.
		// 1073741824.0f is 2^30
		int n = (x * DiamondSquare.xCon + y* DiamondSquare.yCon + this.seed)  & 0x7fffffff;
		n  = ( n  >> 13) ^ n;
		return 1.0f - ((n*(n*n*60493+19990303) + 1376312589) & 0x7fffffff)/1073741824.0f;
	}
}

[System.Serializable]
public class DiamondAttributes{
	public float H = 200f;
	public float scale = 0.01f;
	public float maxHeight = 200f;
}